import ReactDOM from "react-dom";
import $ from 'jquery';
import '@fortawesome/fontawesome-free/css/all.min.css';
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "./../css/login.css";
import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody, MDBInput, MDBBtn } from 'mdbreact';

let csrftoken = $('.token').data('csrftoken');
let pathlogin = $('.pathlogin').data('pathlogin');
const FormPage = () => {
    return (
        <MDBContainer className="offset-md-4">
            <MDBRow>
                <MDBCol md="6">
                    <MDBCard>
                        <MDBCardBody className="mx-4">
                            <div className="text-center">
                                <h3 className="dark-grey-text mb-5">
                                    <strong>Authentification</strong>
                                </h3>
                            </div>
                            <form action={pathlogin} method="post">
                            <MDBInput
                                label="Identifiant"
                                group
                                type="text"
                                validate
                                error="wrong"
                                success="right"
                                name="_username"
                                className="form-group"
                            />
                            <MDBInput
                                label="Mot de passe"
                                group
                                type="password"
                                validate
                                containerClass="mb-0"
                                name="_password"
                                className="form-group"
                            />
                            <input type="hidden" name="_csrf_token" value={csrftoken}/>
                            <div className="text-center mb-3">
                                <MDBBtn
                                    type="submit"
                                    gradient="blue"
                                    rounded
                                    className="btn-block z-depth-1a"
                                >
                                    Se connecter
                                </MDBBtn>
                            </div>
                            </form>
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
            </MDBRow>
        </MDBContainer>
    );
};

ReactDOM.render(<FormPage />, document.getElementById("login"));