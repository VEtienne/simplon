import * as React from "react";
import { List,ReferenceInput,SelectInput,Edit, SimpleForm,Create, Datagrid,Show,SimpleShowLayout, ReferenceField, TextField, EditButton, ShowButton } from 'react-admin';
import ReactDOM from "react-dom";
import { HydraAdmin } from "@api-platform/admin";
import CreateGuesser from "@api-platform/admin/lib/CreateGuesser";
import ResourceGuesser from "@api-platform/admin/lib/ResourceGuesser";
import InputGuesser from "@api-platform/admin/lib/InputGuesser";
import ListGuesser from "@api-platform/admin/lib/ListGuesser";
import EditGuesser from "@api-platform/admin/lib/EditGuesser";
import FieldGuesser from "@api-platform/admin/lib/FieldGuesser";
import polyglotI18nProvider from 'ra-i18n-polyglot';
import frenchMessages from 'ra-language-french';
import $ from 'jquery';
import ShowGuesser from "@api-platform/admin/lib/ShowGuesser";



const i18nProvider = polyglotI18nProvider(() => frenchMessages, 'fr');

//Visiteur
const VisiteurCreate = props => (
    <CreateGuesser {...props}>
        <InputGuesser source="nom" />
    </CreateGuesser>
);

const VisiteurList = props => (
    <ListGuesser {...props}>
        <FieldGuesser source={"nom"} />
        <FieldGuesser source={"createdAt"} showTime={"true"} label={"Création"} />
        <FieldGuesser source={"updatedAt"} showTime={"true"} label={"Mis à jour"} />
    </ListGuesser>
);

const VisiteurEdit = props => (
    <EditGuesser {...props}>
        <InputGuesser source={"nom"} />
    </EditGuesser>
);

const VisiteurShow = props => (
    <ShowGuesser {...props}>
        <FieldGuesser source={"nom"} addLabel={true}/>
        <FieldGuesser source={"createdAt"} showTime={"true"} label={"Création"} addLabel={true}/>
        <FieldGuesser source={"updatedAt"} showTime={"true"} label={"Mis à jour"} addLabel={true}/>>
    </ShowGuesser>
);

//Poste

const PosteCreate = props => (
    <CreateGuesser {...props}>
        <InputGuesser source="libelle" />
    </CreateGuesser>
);

const PosteList = props => (
    <ListGuesser {...props}>
        <FieldGuesser source={"libelle"} />
        <FieldGuesser source={"disponible"} />
        <FieldGuesser showTime={"true"} source={"createdAt"} label={"Création"} />
        <FieldGuesser source={"updatedAt"} showTime={"true"} label={"Mis à jour"} />
    </ListGuesser>
);

const PosteEdit = props => (
    <EditGuesser {...props}>
        <InputGuesser source={"libelle"} />
    </EditGuesser>
);

const PosteShow = props => (
    <ShowGuesser {...props}>
        <FieldGuesser source={"libelle"} addLabel={true}/>
        <FieldGuesser source={"disponible"} addLabel={true}/>
        <FieldGuesser showTime={"true"} source={"createdAt"} label={"Création"} addLabel={true}/>
        <FieldGuesser source={"updatedAt"} showTime={"true"} label={"Mis à jour"} addLabel={true}/>
    </ShowGuesser>
);

//Attribution
const AttributionCreate = props => (
    <Create {...props}>
        <SimpleForm>
        <InputGuesser source="debut" />
        <InputGuesser source="fin" />
        <ReferenceInput addLabel={true} source="visiteur" reference="visiteurs">
            <SelectInput optionText="nom" />
        </ReferenceInput>
        <ReferenceInput addLabel={true} source="poste" reference="postes">
            <SelectInput optionText="libelle" />
        </ReferenceInput>
        </SimpleForm>
    </Create>
);

const AttributionList = props => (
    <List {...props}>
        <Datagrid>
            <FieldGuesser source="debut" addLabel={true} showTime={"true"}/>
            <FieldGuesser source="fin" addLabel={true} showTime={"true"}/>
            <ReferenceField addLabel={true} source="visiteur" reference="visiteurs">
                <TextField source="nom" />
            </ReferenceField>
            <ReferenceField addLabel={true} source="poste" reference="postes">
                <TextField source="libelle" />
            </ReferenceField>
            <FieldGuesser source={"createdAt"}  label={"Création"} addLabel={true} showTime={"true"}/>
            <FieldGuesser source={"updatedAt"}  label={"Mis à jour"} addLabel={true} showTime={"true"}/>
            <ShowButton />
            <EditButton/>
        </Datagrid>
    </List>
);

const AttributionEdit = props => (
    <Edit {...props}>
        <SimpleForm>
        <InputGuesser source="debut" />
        <InputGuesser source="fin" />
            <ReferenceInput addLabel={true} source="visiteur" reference="visiteurs">
                <SelectInput optionText="nom" />
            </ReferenceInput>
            <ReferenceInput addLabel={true} source="poste" reference="postes">
                <SelectInput optionText="libelle" />
            </ReferenceInput>
        <InputGuesser source="erreur" />
        </SimpleForm>
    </Edit>
);

const AttributionShow = props => (
    <Show {...props}>
        <SimpleShowLayout>
            <FieldGuesser source="debut" addLabel={true} showTime={"true"}/>
            <FieldGuesser source="fin" addLabel={true} showTime={"true"}/>
            <ReferenceField addLabel={true} source="visiteur" reference="visiteurs">
                <TextField source="nom" />
            </ReferenceField>
            <ReferenceField addLabel={true} source="poste" reference="postes">
                <TextField source="libelle" />
            </ReferenceField>
            <FieldGuesser source={"createdAt"}  label={"Création"} addLabel={true} showTime={"true"}/>
            <FieldGuesser source={"updatedAt"}  label={"Mis à jour"} addLabel={true} showTime={"true"}/>
        </SimpleShowLayout>
    </Show>
);

export const lightTheme = {
    palette: {
        background: {
            paper: "#fff",
            default: "#fff"
        },
        primary: {
            main: '#607D8B'
        },
        secondary: {
            main: '#607D8B'
        },
        sidebarColor: '#E7ECEE',
        menuItemsColor: '#4B6471'
    },

// overrides default theme css
    overrides: {
        // targeting refresh button
        RaAppBar: {
            toolbar: {
                '& svg': {
                    '&:not(:nth-child(1))': {
                        display: 'none'
                    }
                }
            }
        }
    }
};
$(document).on('click', '.logout', function () {
    window.location="http://www.simplon.etienne-vaytilingom.re/logout"
});

ReactDOM.render(
    <HydraAdmin theme={lightTheme} i18nProvider={i18nProvider} entrypoint="http://www.simplon.etienne-vaytilingom.re/api">
        <ResourceGuesser name="visiteurs" create={VisiteurCreate} edit={VisiteurEdit} list={VisiteurList} show={VisiteurShow}/>
        <ResourceGuesser name="postes" create={PosteCreate} edit={PosteEdit} list={PosteList} show={PosteShow}/>
        <ResourceGuesser name="attributions"  list={AttributionList} show={AttributionShow} create={AttributionCreate} edit={AttributionEdit}/>
    </HydraAdmin>

    ,document.getElementById('react'));