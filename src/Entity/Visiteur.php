<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource
 * @ORM\Entity
 */
class Visiteur
{
    use TimestampableEntity;
    use BlameableEntity;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column
     * @Assert\NotBlank
     */
    public $nom;


    /** occupe un poste ou non
     * @ORM\Column(type="boolean")
     */
    public $occupe=false;

    /**
     * @var Attribution[]
     *
     * @ORM\OneToMany(targetEntity="Attribution", mappedBy="visiteur", cascade={"persist"})
     */
    public $attributions;

    public function getId(): ?int
    {
        return $this->id;
    }

}