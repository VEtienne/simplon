<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Blameable\Traits\BlameableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource
 * @ORM\Entity(repositoryClass="App\Repository\AttributionRepository")
 */
class Attribution
{
    use TimestampableEntity;
    use BlameableEntity;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     */
    public $debut;

    /**
     * @var \DateTime
     * @ORM\Column (type="datetime")
     * @Assert\NotBlank
     */
    public $fin;

     /**
     * @ORM\ManyToOne(targetEntity="Visiteur", inversedBy="attributions")
     */
    public $visiteur;

    /**
     * @ORM\ManyToOne(targetEntity="Poste", inversedBy="attributions")
     */
    public $poste;

    /**
     * @ORM\Column(type="boolean")
     */
    public $init=false;

    /**
     * @ORM\Column(type="boolean")
     */
    public $termine=false;

    /**
     * @ORM\Column(type="boolean")
     */
    public $erreur=false;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDebut(): \DateTime
    {
        return $this->debut;
    }

    /**
     * @param \DateTime $debut
     */
    public function setDebut(\DateTime $debut): void
    {
        $this->debut = $debut;
    }

    /**
     * @return \DateTime
     */
    public function getFin(): \DateTime
    {
        return $this->fin;
    }

    /**
     * @param \DateTime $fin
     */
    public function setFin(\DateTime $fin): void
    {
        $this->fin = $fin;
    }



}