<?php

namespace App\Repository;


use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use App\Entity\Attribution;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;

class AttributionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Attribution::class);
    }

    public function Poste_Between_Disponible($now)
    {
        $qb = $this->createQueryBuilder('t');
        $qb
            ->add('where',$qb->expr()->between(
                ':n','t.debut','t.fin'
            ))
            ->andWhere('t.init = false')
            ->andWhere('t.erreur = false')
            ->setParameter('n',$now)
        ->orderBy('t.createdAt','ASC');

        return $qb ->getQuery()
            ->getResult();
    }

    public function Poste_NotBetween_NonDisponible($now)
    {
        $qb = $this->createQueryBuilder('t');
        $qb->leftJoin('t.poste','p')
            ->leftJoin('t.visiteur','v')
            ->add('where',$qb->expr()->not($qb->expr()->between(
                ':n','t.debut','t.fin'
            )))
            ->andWhere('t.init = true')
            ->andWhere('t.erreur = false')
            ->andWhere('t.termine = false')
            ->setParameter('n',$now);

        return $qb ->getQuery()
            ->getResult();
    }
}