<?php
namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Attribution;
use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class AttributionSubscriber implements EventSubscriberInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->em = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['updateAttribution', EventPriorities::POST_WRITE],
        ];
    }

    public function updateAttribution(ViewEvent $event): void
    {
        $attribution = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();



        if (!$attribution instanceof Attribution || Request::METHOD_POST !== $method) {
            return;
        }
        $attribution->setDebut($attribution->getDebut()->add(new DateInterval('PT4H')));
        $attribution->setFin($attribution->getFin()->add(new DateInterval('PT4H')));
        $this->em->persist($attribution);
        $this->em->flush();
        $Attributions = $this->em->getRepository('App:Attribution')->Poste_NotBetween_NonDisponible(new \DateTime());
        foreach ($Attributions as $a){
            $a->termine = true;
            $a->poste->disponible = true;
            $a->visiteur->occupe = false;
            $this->em->persist($a->visiteur);
            $this->em->persist($a->poste);
            $this->em->persist($a);
        }
        $Attributions = $this->em->getRepository('App:Attribution')->Poste_Between_Disponible(new \DateTime());
        foreach ($Attributions as $a){
            if($a->poste->disponible and !$a->visiteur->occupe) {
                $a->poste->disponible = false;
                $a->visiteur->occupe = true;
                $a->init=true;
                $this->em->persist($a->visiteur);
                $this->em->persist($a->poste);
                $this->em->persist($a);
            }

            else {
                $a->erreur = true;
                $this->em->persist($a);
            }
        }
        $this->em->flush();
    }
}