<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;

class UpdatePosteCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:UpdatePoste';

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // ... put here the code to run in your command

        // this method must return an integer number with the "exit status code"
        // of the command. You can also use these constants to make code more readable

        // return this if there was no problem running the command
        // (it's equivalent to returning int(0))
        $now = new \DateTime();

        $Attributions = $this->em->getRepository('App:Attribution')->Poste_NotBetween_NonDisponible($now);
        foreach ($Attributions as $a){
            $a->termine = true;
            $a->poste->disponible = true;
            $a->visiteur->occupe = false;
            $this->em->persist($a->visiteur);
            $this->em->persist($a->poste);
            $this->em->persist($a);
        }

        $Attributions = $this->em->getRepository('App:Attribution')->Poste_Between_Disponible($now);
        foreach ($Attributions as $a){
            if($a->poste->disponible and !$a->visiteur->occupe) {
                $output->writeln(['Attribution poste:' . $a->poste->libelle . ' à ' . $a->visiteur->__tostring()]);
                $a->poste->disponible = false;
                $a->visiteur->occupe = true;
                $a->init=true;
                $this->em->persist($a->visiteur);
                $this->em->persist($a->poste);
                $this->em->persist($a);
            }

            else {
                $output->writeln(
                    ['Erreur Attribution poste:' . $a->poste->libelle . ' à ' . $a->visiteur->__tostring() . ':'
                    . $a->poste->disponible ? 'Poste disponible' : 'Poste non disponible' . ' -- '
                    . $a->visiteur->occupe ? 'visiteur occupe' : 'visiteur non occupe']);
                $a->erreur = true;
                $this->em->persist($a);
            }
        }
        $this->em->flush();
        return 0;

        // or return this if some error happened during the execution
        // (it's equivalent to returning int(1))
        // return Command::FAILURE;
    }
}
