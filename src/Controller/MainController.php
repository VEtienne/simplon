<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/admin", name="homepage")
     */
    public function index()
    {
        return $this->render('admin/admin.html.twig');
    }

    /**
     * @Route("/", name="welcome")
     */
    public function welcome()
    {
        return $this->redirectToRoute('homepage');
    }
}