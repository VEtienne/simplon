<?php

namespace App\Form\Handler;

use App\Entity\User;
use App\Model\User as UserModel;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var LoggerInterface
     */
    private $loggerInterface;

    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $loggerInterface)
    {
        $this->entityManager = $entityManager;
        $this->loggerInterface = $loggerInterface;
    }

    public function handle(FormInterface $form, Request $request, UserPasswordEncoderInterface $encoder)
    {
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            /**
             * @var UserModel $userModel
             */
            $userModel = $form->getData();

            /**
             * @var User $user
             */
            $user = new User();
            $user->setUsername($userModel->username);
            $user->setEmail($userModel->email);
            $user->setRoles(['ROLE_ADMIN']);
            $passEncoded = $encoder->encodePassword($user, $userModel->password);
            $user->setPassword($passEncoded);

            try {
                $this->entityManager->persist($user);
            } catch (ORMException $e) {
                $this->loggerInterface->error($e->getMessage());
                $form->addError(new FormError('Erreur lors de l\'insertion en base de l\'utilisateur...'));
                return false;
            }

            $this->entityManager->flush();

            return true;
        }

        return false;
    }
}
